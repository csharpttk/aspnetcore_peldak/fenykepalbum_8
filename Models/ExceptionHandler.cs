﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace fenykepalbum_8.Models
{
    
        public class KöztesKivételkezelő
        {
            private readonly RequestDelegate _következő;

            public KöztesKivételkezelő(RequestDelegate következő)
            {
                _következő = következő;
            }

            public async Task Invoke(HttpContext httpContext)
            {
            try
            {
                await _következő(httpContext);
            }
            catch (Exception exc)
            {
                string hibaszöveg="Ismeretlen hiba";
                switch (exc.GetType().ToString()){
                    case "System.OutOfMemoryException": hibaszöveg = "Nincs t&ouml;bb mem&oacute;ria";break;                
                    case "System.FormatException": hibaszöveg = "Nem megfelel&ocirc; form&aacute;tum"; break;
                    case "System.IndexOutOfRangeException": hibaszöveg = "Indexel&eacute;si hiba"; break;                     
                }
               
                
                await httpContext.Response.WriteAsync("<div style='text-align:center'><h1>"+hibaszöveg+"</h1></div>");
               
                throw;
            }
        }
        }
    
}
