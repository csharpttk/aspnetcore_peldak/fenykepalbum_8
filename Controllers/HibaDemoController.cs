﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace fenykepalbum_8.Controllers
{
    public class HibaDemoController : Controller
    {
        public IActionResult Index()
        {
            
            return View("HibaDemo");
        }
        public IActionResult IndexHiba()
        {
            int[] x = new int[5];
            x[11] = 17;  //csak 5-ig indexelhetném
            return View("Rendben");
        }
        public IActionResult FormátumHiba()
        {
            int x = Int32.Parse("alma");  //nem szám
            return View("Rendben");
        }
        public IActionResult MemóriaHiba()
        {
            double[] x = new double[int.MaxValue]; //várhatóan ez túl sok lesz
            return View("Rendben");
        }
        public IActionResult IsmeretlenHiba()
        {
            throw new Exception();  //ha megjegyzésbe teszi, látszik, hogy lefut
            return View("Rendben");
        }
        
    }
}