﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using fenykepalbum_8.AspNetCore.NewDb.Models;
using fenykepalbum_8.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace fenykepalbum_8.Controllers
{
    
    public class Filekezeles : Controller
    {

        private readonly fenykepContext _context;
        public Filekezeles(fenykepContext context)
        {
            _context = context;
        }
        [Authorize(Roles = "admin")]
        public IActionResult Index(string szűrő)
        {
            if (szűrő is null)
            {
                szűrő = "";
            }
            ViewData["szűrő"] = szűrő;
            Fileok fileok = new Fileok("wwwroot/kepek/", "feltöltésideje DESC");
            ViewData["fileok"] = fileok.fnevekidők.Where(x=>x.filenév.Contains(szűrő)).ToList();
            return View();
        }
        [Authorize(Roles ="admin")]
        public IActionResult Töröl(string törlendő)
        {
            ViewData["adatbázisbanBejegyezve"] = _context.Kepek.Where(X => X.Filenev == törlendő).Count() > 0;         
            ViewData["törlendő"] = törlendő;
            return View();
        }
        [Authorize(Roles = "admin")]
        public IActionResult IgenTörlés(string törlendő)
        {
            Fájl törlendőf = new Fájl(törlendő);
            
            ViewData["siker"] = törlendőf.Töröl();
           
            ViewData["törlendő"] = törlendő;
            return View();
        }
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
    
                List<string> márfeltöltöttek = new Fileok("wwwroot/kepek/").fnevek;
                
                if (file == null || file.Length == 0)
                    return Content("Nincs kiválasztott fájl!");

                if (márfeltöltöttek.Contains(file.FileName))
                {
                    ViewData["márvolt"] = true;
                    throw new Exception();
                }
                ViewData["filenev"] = file.FileName;
                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot/kepek",
                            file.FileName);

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
                ViewData["siker"] = true;
           
            return View();        
        }

    }
}